<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php include("metodos.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
<title>Sunny Weather Widget  Flat Responsive Widget Template :: w3layouts</title>
<!-- Custom Theme files -->
<link href="css/Plantilla2.css" rel="stylesheet" type="text/css" media="all"/>
<!-- for-mobile-apps -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="keywords" content="Sunny Weather Widget Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- //for-mobile-apps -->

<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Gudea:400,700' rel='stylesheet' type='text/css'>
</head>
<body>
<!--widget start here-->
<div class="widget">
	<div class="wrap">
		   <h1>Sunny Weather Widget</h1>
		<div class="widget-main">
		   <div class="widget-climate-wthree">
				<div class="widget-left">
					<ul id="otros-municipios">
				</ul>
				</div>		
				<div class="widget-right w3l">
					<p id="recibeTemperatura"></p>
					<p>May 2016</p>
					<h5>Mostly Sunny</h5>
					
					<h2 ></h2>
				</div>
			 <div class="clear"> </div>				
		  </div>
		</div>
	</div>
</div>
<!--wedget end here-->
<!--copy rights end here-->
<div class="copy-rights">		 	
	<p>© 2016 Sunny Weather Widget. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>		 	
</div>
<!--copyrights start here-->

</body>
</html>