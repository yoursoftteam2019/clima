<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php include("metodos.php"); ?>
<!DOCTYPE html>
<html>
<head>
<title>Sunlight Weather Widget Flat Responsive Widget Template :: w3layouts</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Sunlight Weather Widget Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="js/cargaJQuery.js" type="text/javascript"></script>
<link href="css/stylePlantilla2.css" rel="stylesheet" type="text/css" media="all" />
<link href='//fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
</head>
<body onload="startTime()">
	<!-- main -->
		<div class="main">
			<h1>Sunlight Weather Widget</h1>
			<div class="main-grid1">
				<div class="main-grid1-pos">
					<span> </span>
				</div>
				<h2><p id="recibeTemperatura">

					</p></h2>
				<p>Sabaneta</p>
				<div class="main-grid1-grids">
					<div class="main-grid1-grid-left">
						<p><sup class="degree"></sup> <sup class="degree"></sup></p>
						<h3></h3>
					</div>
					<div class="main-grid1-grid-right" id="txt">
						<script type="text/javascript">
							var mydate=new Date()
							var year=mydate.getYear()
							if(year<1000)
								year+=1900
							var day=mydate.getDay()
							var month=mydate.getMonth()
							var daym=mydate.getDate()
							if(daym<10)
								daym="0"+daym
							var dayarray=new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado")
							var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")
							document.write(""+dayarray[day]+", "+montharray[month]+" "+daym+", "+year+"")
						</script>
					</div>
					<div class="clear"> </div>
				</div>
			</div>
			<div class="main-grid2">
				<div class="main-grid2-left">
					<ul id="otros-municipios"></ul>
				</div>
				<div class="main-grid2-left">
					<img src="images/cloudy.png" alt=" " class="img-responsive" />
					<p>Mon <span>18 / 6<sup class="degree">°</sup></span></p>
				</div>
				<div class="main-grid2-left">
					<img src="images/cloudy.png" alt=" " class="img-responsive" />
					<p>Tue <span>16 / 4<sup class="degree">°</sup></span></p>
				</div>
				<div class="main-grid2-left">
					<img src="images/2.png" alt=" " class="img-responsive" />
					<p>Sat <span>11 / 7<sup class="degree">°</sup></span></p>
				</div>
				<div class="clear"> </div>
			</div>
			<div class="footer">
				<p>Copyright © 2015 Sunlight Weather Widget. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
			</div>			
		</div>
		<script>
		function startTime() {
			var today = new Date();
			var h = today.getHours();
			var m = today.getMinutes();
			var s = today.getSeconds();
			m = checkTime(m);
			s = checkTime(s);
			document.getElementById('txt').innerHTML =
			h + ":" + m + ":" + s;
			var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
				if (i < 10) {i = "0" + i}; // add zero in front of numbers < 10
				return i;
			}
		</script>
</body>
</html>