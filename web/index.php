<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php include("metodos.php"); ?>
<!DOCTYPE html>
<html>



<!-- Head -->
<head>

	<title>Clima municiopios antioqueños</title>

	<!-- For-Mobile-Apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="New York Weather Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //For-Mobile-Apps -->

	<!-- Style --> <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />

	<!-- Web-Fonts -->
	<link href='//fonts.googleapis.com/css?family=Oxygen:400,300,700' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Raleway:100,200' rel='stylesheet' type='text/css'>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="js/cargaJQuery.js" type="text/javascript"></script>
	<!-- //Web-Fonts -->

</head>
<!-- Head -->



<!-- Body -->
<body onload="startTime()">
	<!-- Container -->
	<div class="container container-body">

		<!-- City -->
		<div class="city">
			<div class="title">
				<h1>Sabaneta</h1>
				<h3>Antioquia</h3>
			</div>
			<div class="date-time">
				<div class="dmy">
					<div id="txt"></div>
					<div class="date">
						<!-- Date-JavaScript -->
						<script type="text/javascript">
							var mydate=new Date()
							var year=mydate.getYear()
							if(year<1000)
								year+=1900
							var day=mydate.getDay()
							var month=mydate.getMonth()
							var daym=mydate.getDate()
							if(daym<10)
								daym="0"+daym
							var dayarray=new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado")
							var montharray=new Array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre")
							document.write(""+dayarray[day]+", "+montharray[month]+" "+daym+", "+year+"")
						</script>
						<!-- //Date-JavaScript -->
					</div>
				</div>
				<div class="temperature">
					<p id="recibeTemperatura">

					</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<!-- //City -->



		<!-- Forecast -->
		<div class="forecast">
			<!--<div  id="estadoTiempoSabaneta"></div>-->
			<div class="forecast-icon" id="imgTiempoSabaneta">
				<img src="" alt="Sabaneta Weather Widget" class="forecast-image">
			</div> 
			<div class="today-weather" id="estadoTiempoSabaneta">
				<h3></h3>
			</div>
			<div class="today-weather">
				<ul id="otros-municipios">
				</ul>
			</div>
		</div>
		<!-- //Forecast -->
		<div class="clear"></div>

	</div>
	<!-- //Container -->



	<!-- Footer -->
	<div class="footer">

		<!-- Copyright -->
		<div class="copyright">
			<p> YourSoftTeam 2019</p>
		</div>
		<!-- //Copyright -->

	</div>
	<!-- //Footer -->



	<!-- Custom-JavaScript-Files -->

	<!-- Time-JavaScript -->
	<script>
		function startTime() {
			var today = new Date();
			var h = today.getHours();
			var m = today.getMinutes();
			var s = today.getSeconds();
			m = checkTime(m);
			s = checkTime(s);
			document.getElementById('txt').innerHTML =
			h + ":" + m + ":" + s;
			var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
				if (i < 10) {i = "0" + i}; // add zero in front of numbers < 10
				return i;
			}
		</script>
		<!-- //Time-JavaScript -->

		<!-- //Custom-JavaScript-File-Links -->



	</body>
	<!-- //Body -->



	</html>