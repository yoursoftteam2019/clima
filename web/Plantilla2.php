<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php include("metodos.php"); ?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Condiciones del clima Sabaneta</title>
	<!-- Custom Theme files -->
	<link href="css/Plantilla2.css" rel="stylesheet" type="text/css" media="all"/>
	<!-- for-mobile-apps -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
	<meta name="keywords" content="Sunny Weather Widget Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<!-- //for-mobile-apps -->

	<!--Google Fonts-->
	<link href='//fonts.googleapis.com/css?family=Gudea:400,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="js/cargaJQuery.js" type="text/javascript"></script>
</head>
<body>
	<!--widget start here-->
	<div class="widget" style="padding-top: 10%; height: 80%;width: 100%;">
		<div class="wrap">
			<div class="widget-main">
				<div style="height: 100%;width: 100%;">
					<div class="widget-left">
						<ul style="font-family: 'Roboto', sans-serif;padding-bottom: 30px;" id="otros-municipios">
						</ul>
						<div class="forecast-icon" id="imgTiempoSabaneta">
							<img src="" alt="Sabaneta Weather Widget" class="forecast-image">
						</div>
					</div>		
					<div class="widget-right w3l" id="estadoTiempoSabaneta">
						<p style="font-size: 6vw;padding-bottom: 30%">Sabaneta</p>
						<p style="font-size: 4vw" id="recibeTemperatura"></p>
						<p style="font-size: 1.5vw;">
							<script>
								var meses = new Array ("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
								var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
								var f=new Date();
								document.write(diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
							</script>
						</p>
						<h3 style="font-size: 30px;color: #ffffff"></h3>
						
						<h2></h2>
					</div>
					<div class="clear"> </div>				
				</div>
			</div>
		</div>
	</div>
<div class="footer-clima">
		<p>© 2019 Yoursoft Team. All Rights Reserved | Design by
			<a href="http://Yoursoft.com/" target="_blank">Yoursoft</a>
		</p>
		</div>

</body>
</html>